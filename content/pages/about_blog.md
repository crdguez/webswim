Title: Sobre el blog

Este blog se ha alojado en [gitlab pages](https://docs.gitlab.com/ee/user/project/pages/) usando el gestor estático de contenidos [Pelican](https://getpelican.com/).

Todo el código del blog se encuentra disponible en el repositorio [https://gitlab.com/crdguez/webswim](https://gitlab.com/crdguez/webswim)

El blog se ha automatizado en la medida de lo posible usando *python*, en concreto jupyter notebooks:

* [https://gitlab.com/crdguez/webswim/-/blob/main/content/media/creando_posts.ipynb](https://gitlab.com/crdguez/webswim/-/blob/main/content/media/creando_posts.ipynb)

* [https://gitlab.com/crdguez/webswim/-/blob/main/content/media/probando_manipular_videos.ipynb](https://gitlab.com/crdguez/webswim/-/blob/main/content/media/probando_manipular_videos.ipynb)
