Title: Trofeo Agustinos 2022-2023
Date: 2023/05/28
Category: Natación
Tags: Marcelo, Natación

El pasado 2023/05/28 se celebró en P. San Agustin la competición **Trofeo Agustinos** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **50m Espalda** hizo 38.94 quedándose 5º
 * En **50m Libre** hizo 31.84 quedándose 2º
 


Aquí os dejamos alguna foto: 

 ![Trofeo Agustinos]({static}/media/2223/Image_2023-05-28_at_173828.jpeg){width=50%}
 ![Trofeo Agustinos]({static}/media/2223/Image_2023-05-28_at_132856.jpeg){width=50%}
 ![Trofeo Agustinos]({static}/media/2223/Image_2023-05-28_at_173829.jpeg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/98qemCfwxrQ
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Xk8dIc-KWh8
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.