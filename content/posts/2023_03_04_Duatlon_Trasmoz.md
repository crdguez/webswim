Title: Duatlón Escolar de Trasmoz 23
Date: 2023/04/30
Category: Triatlón
Tags: Marcelo, Carla, Triatlón

El pasado 2023/03/04 se celebró  el **Duatlón Escolar de Trasmoz** donde participaron Carla y Marcelo.

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/RqzM0DZiF9Y
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.