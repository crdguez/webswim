Title: Trofeo Fondo y Estilos 2024-2025
Date: 2024/10/26
Category: Natación
Tags: Marcelo, Natación

El pasado 2024/10/26 se celebró en St. Casablanca la competición **Trofeo Fondo y Estilos** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **1500m Libre** hizo 19:17.51 quedándose 34º
 * En **400m Estilos** hizo 5:22.34 quedándose 27º
 


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/MFaopAWWCzY
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/bEaka-AONx8
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.