Title: Alevín, Campeonato de Aragón 2023-2024
Date: 2024/02/24
Category: Natación
Tags: Marcelo, Natación

El pasado 2024/02/24 se celebró en St. Casablanca la competición **Alevín, Campeonato de Aragón** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **200m Libre** hizo 2:19.99 quedándose 1º
 * En **100m Libre** hizo 1:04.19 quedándose 1º
 * En **100m Mariposa** hizo 1:10.78 quedándose 1º
 * En **200m Estilos** hizo 2:39.36 quedándose 1º
 


Aquí os dejamos alguna foto: 

 ![Alevín, Campeonato de Aragón]({static}/media/2324/IMG-20240225-WA0104.jpg){width=50%}
 ![Alevín, Campeonato de Aragón]({static}/media/2324/IMG-20240225-WA0105.jpg){width=50%}
 ![Alevín, Campeonato de Aragón]({static}/media/2324/IMG-20240225-WA0106.jpg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/h8hwXhsE6uE
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/vpcUjfG1vig
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/qDpD8XeTPQk
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/eE6XyYebRhc
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.