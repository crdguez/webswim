Title: Benjamín, jornada 5 2022-2023
Date: 2023/03/26
Category: Natación
Tags: Marcelo, Natación

El pasado 2023/03/26 se celebró en E.M. El Olivar la competición **Benjamín, jornada 5** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **200m Espalda** hizo 3:05.98 quedándose 9º
 * En **100m Mariposa** hizo 1:22.89 quedándose 2º
 


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/1PQ7viqCGhk
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/JG7Wv54azjk
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.