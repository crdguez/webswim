Title: Trofeo Internacional Castalia Castelló 2024-2025
Date: 2024/12/07
Category: Natación
Tags: Marcelo, Natación

El pasado 2024/12/07 se celebró en Castellón la competición **Trofeo Internacional Castalia Castelló** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **200m Estilos** hizo 2:28.74 quedándose 44º
 


Aquí os dejamos alguna foto: 

 ![Trofeo Internacional Castalia Castelló]({static}/media/2425/IMG_20241209_093855_022.jpg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/S0JIYSucDRw
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/USbXkVaOi9I
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.