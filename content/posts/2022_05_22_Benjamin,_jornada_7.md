Title: Benjamín, jornada 7 2021-2022
Date: 2022/05/22
Category: Natación
Tags: Marcelo, Natación

El pasado 2022/05/22 se celebró en C.N. Helios la competición **Benjamín, jornada 7** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **200m Estilos** hizo 3:13.06 quedándose 5º
 


Nos vemos en la próxima, y recuerdaaaaa: **¡Carpe diem!**.