Title: La función de Navidad 2023-2024
Date: 2023/12/18
Category: Miscelánea
Tags: Carla, Miscelánea

El pasado 2022/12/18 estuvimos en **La función de Navidad** de Carla.

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/bBQHXuuzgCI
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>



Nos vemos en la próxima.