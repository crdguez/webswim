Title: Infantil, Campeonato  de Aragón 2023-2024
Date: 2024/06/28
Category: Natación
Tags: Marcelo, Natación

El pasado 2024/06/28 se celebró en ST. VENECIA la competición **Infantil, Campeonato  de Aragón** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **200m Libre** hizo 2:22.23 quedándose 2º
 * En **100m Espalda** hizo 1:15.80 quedándose 2º
 * En **100m Braza** hizo 1:24.77 quedándose 1º
 


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/111iERq6Ot8
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/U-QqV9MYMNU
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/zmWzXwhjOeg
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.