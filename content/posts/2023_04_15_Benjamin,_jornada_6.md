Title: Benjamín, jornada 6 2022-2023
Date: 2023/04/15
Category: Natación
Tags: Marcelo, Natación

El pasado 2023/04/15 se celebró en E.M. El Olivar la competición **Benjamín, jornada 6** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **400m Libre** hizo 5:35.09 quedándose 2º
 


Aquí os dejamos alguna foto: 

 ![Benjamín, jornada 6]({static}/media/2223/IMG_20230415_161600.jpg){width=50%}
 ![Benjamín, jornada 6]({static}/media/2223/1.jpg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/6F1tm-RBaxY
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.