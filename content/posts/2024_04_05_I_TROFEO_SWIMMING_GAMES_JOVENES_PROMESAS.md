Title: I TROFEO SWIMMING GAMES JÓVENES PROMESAS 2023-2024
Date: 2024/04/05
Category: Natación
Tags: Marcelo, Natación

El pasado 2024/04/05 se celebró en E.M. El Olivar la competición **I TROFEO SWIMMING GAMES JÓVENES PROMESAS** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **100m Espalda** hizo 1:15.80 quedándose 4º
 * En **100m Mariposa** hizo 1:12.15  quedándose 2º
 * En **200m Estilos** hizo 2:37.24 quedándose 1º
 


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/tmfWd2tMQ1I
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Zx4S1TctsLk
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/-EcHtHL0zPU
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.