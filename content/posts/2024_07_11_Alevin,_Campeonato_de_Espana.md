Title: Alevín, Campeonato de España 2023-2024
Date: 2024/07/11
Category: Natación
Tags: Marcelo, Natación

El pasado 2024/07/11 se celebró en Ciudad Real la competición **Alevín, Campeonato de España** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **400m Estilos** hizo 5:34.61 quedándose 21º
 * En **200m Estilos** hizo 2:35.91 quedándose 16º
 * En **200m Mariposa** hizo 2:36.62 quedándose 11º
 

Aquí puedes ver todos los resultados: [Resultados]({static}/media/2324/resultados_completos_campeonato_espana.pdf)

Aquí os dejamos alguna foto: 

 ![Alevín, Campeonato de Espala]({static}/media/2324/IMG_20241209_095012_715.jpg){width=50%}
 ![Alevín, Campeonato de Espala]({static}/media/2324/IMG_20241209_094939_276.jpg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/7WRlHKinuNw
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/FFbInMwyFNs
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/_O2EfBVEoNs
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.