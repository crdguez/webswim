Title: San Silvestre 2024
Date: 2024/12/31
Category: Atletismo	
Tags: Marcelo, Carla, Atletismo

El pasado 2024/12/31 estuvimos en **Sabiñanigo** corriendo la popular San Silvestre.

Los resultados fueron: 

* **Carla** quedó 1ª de categoría alevín
* **Marcelo** quedó 4º de categoría cadete


Aquí os dejamos alguna foto: 

![Podio completo]({static}/media/2425/IMG_20241231_185352.jpg){width=50%}
![Carla podio]({static}/media/2425/IMG_20241231_183859.jpg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/5C50r-3QGXY
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.