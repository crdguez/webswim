Title: XXXIII CROSS DE UTEBO 2022-2023
Date: 2022/11/06
Category: Atletismo
Tags: Marcelo, Atletismo

El pasado 2022/11/06 se celebró en Utebo la competición **XXXIII CROSS DE UTEBO** de Atletismo donde participó Marcelo.

ti
Los resultados obtenidos fueron: 

 * En **1100m** hizo 04:33:00 quedándose 25º
 


Aquí os dejamos alguna foto: 

 ![XXXIII CROSS DE UTEBO]({static}/media/2223/IMG_20221106_104654.jpg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/05au6vbFHF0
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.