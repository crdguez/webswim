Title: Benjamín, jornada 1 2022-2023
Date: 2022/11/19
Category: Natación
Tags: Marcelo, Natación

El pasado 2022/11/19 se celebró en Stadium Venecia la competición **Benjamín, jornada 1** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **100m Espalda** hizo 1:27.15 quedándose 5º
 * En **100m Libre** hizo 1:14.69 quedándose 2º
 


Aquí os dejamos alguna foto: 

 ![Benjamín, jornada 1]({static}/media/2223/IMG_20221119_171934.jpg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/dTSSL46IBKY
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/GdEXGTBzqCY
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima, y recuerdaaaaa: **¡Carpe diem!**.