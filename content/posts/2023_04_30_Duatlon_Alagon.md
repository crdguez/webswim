Title: Duatlón Escolar de Alagón 23
Date: 2023/04/30
Category: Triatlón
Tags: Marcelo, Carla, Triatlón

El pasado 2023/04/30 se celebró  el **Duatlón Escolar de Alagón** donde participaron Carla y Marcelo.

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/7B5dhPEKi_0
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.