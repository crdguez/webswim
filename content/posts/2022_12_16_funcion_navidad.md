Title: La función de Navidad 2022-2023
Date: 2022/12/16
Category: Miscelánea
Tags: Marcelo, Carla, Miscelánea

El pasado 2022/12/16 estuvimos en **La función de Navidad** de Carla donde interpretó a Charlie de Charlie y **la fábrica de chocolate** y el 20022/12/20 fue el turno de Marcelo.


Aquí os dejamos alguna foto: 

 ![La función de Navidad]({static}/media/2223/IMG_20221216_154315.jpg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/JvabwvQxLS4
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/6sJ7hqF-ipo
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>


Nos vemos en la próxima.