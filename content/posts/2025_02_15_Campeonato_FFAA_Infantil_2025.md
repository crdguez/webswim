Title: Campeonato FFAA Infantil 2025 2024-2025
Date: 2025/02/15
Category: Natación
Tags: Marcelo, Natación

El pasado 2025/02/15 se celebró en Son Hugo (Palma de Mallorca) la competición **Campeonato FFAA Infantil 2025** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **200m Mariposa** hizo 2:29.98 quedándose 18º
 * En **200m Estilos** hizo 2:32.40 quedándose 23º
 


Aquí os dejamos alguna foto: 

 ![Campeonato FFAA Infantil 2025]({static}/media/2425/IMG-20250215-WA0000.jpg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/qTU9WzDE_nY
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/XU0J9zAvBr0
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.