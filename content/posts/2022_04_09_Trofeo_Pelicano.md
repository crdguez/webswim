Title: Trofeo Pelícano 2021-2022
Date: 2022/04/09
Category: Natación
Tags: Marcelo, Natación

El pasado 2022/04/09 se celebró en P. San Agustin la competición **Trofeo Pelícano** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **50m Mariposa** hizo 38 quedándose 3º
 * En **100m Mariposa** hizo 01:27:00 quedándose 1º
 


Aquí os dejamos alguna foto: 

 ![Trofeo Pelícano]({static}/media/2122/IMG-20220409-WA0015.jpeg){width=50%}
 ![Trofeo Pelícano]({static}/media/2122/photo_2022-12-09_15-17-30.jpg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/bsaF0RQPUTA
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/CQO1U7PEpRY
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima, y recuerdaaaaa: **¡Carpe diem!**.