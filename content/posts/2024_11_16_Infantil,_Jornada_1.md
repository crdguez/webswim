Title: Infantil, Jornada 1 2024-2025
Date: 2024/11/16
Category: Natación
Tags: Marcelo, Natación

El pasado 2024/11/16 se celebró en C.N. Helios la competición **Infantil, Jornada 1** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **400m Libre** hizo 4:50.41 quedándose 6º
 * En **200m Braza** hizo 2:54.97 quedándose 1º
 * En **200m Mariposa** hizo 2:32.78 quedándose 1º
 * En **200m Estilos** hizo 2:32.80 quedándose 4º
 


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/KzVhs7qhv1Q
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/q_ZZ0hPhnsQ
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/dyJELzHiYxM
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Ybc2W-lTFTs
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.