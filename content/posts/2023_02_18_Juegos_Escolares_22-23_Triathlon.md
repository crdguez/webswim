Title: Duatlón Escolar de Tauste 23
Date: 2023/02/18
Category: Triatlón
Tags: Marcelo, Carla, Triatlón

El pasado 2023/02/18 se celebró  el **Duatlón Escolar de Tauste** donde participaron Carla y Marcelo.


Aquí os dejamos alguna foto: 

 ![Juegos Escolares 22-23 Triathlón]({static}/media/2223/IMG_20230218_122102.jpg){width=50%}
 ![Juegos Escolares 22-23 Triathlón]({static}/media/2223/IMG_20230218_130803.jpg){width=50%}
 ![Juegos Escolares 22-23 Triathlón]({static}/media/2223/IMG_20230218_130806.jpg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/2OaXMA5_d4o
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.