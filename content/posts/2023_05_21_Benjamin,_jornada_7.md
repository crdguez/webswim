Title: Benjamín, jornada 7 2022-2023
Date: 2023/05/21
Category: Natación
Tags: Marcelo, Natación

El pasado 2023/05/21 se celebró en E.M. El Olivar la competición **Benjamín, jornada 7** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **200m Estilos** hizo 2:53.62 quedándose 3º
 


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/IScJ_dxRJIk
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.