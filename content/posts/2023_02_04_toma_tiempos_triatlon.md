Title: Toma de Tiempos Triatlón Benjamines 2023
Date: 2023/02/04
Category: Triatlón
Tags: Marcelo, Triatlón

El pasado 2023/02/04 se celebró en la toma de tiempos organizada por la Fatri donde participó Marcelo.


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/95TSA3mmSDM
              " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.