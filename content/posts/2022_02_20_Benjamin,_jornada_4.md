Title: Benjamín, jornada 4 2021-2022
Date: 2022/02/20
Category: Natación
Tags: Marcelo, Natación

El pasado 2022/02/20 se celebró en E.M. El Olivar la competición **Benjamín, jornada 4** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **100m Libre** hizo 1:18.56 quedándose 2º
 * En **100m Braza** hizo 1:48.18 quedándose 7º
 


Nos vemos en la próxima, y recuerdaaaaa: **¡Carpe diem!**.