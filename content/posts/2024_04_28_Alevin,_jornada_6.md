Title: Alevín, jornada 6 2023-2024
Date: 2024/04/28
Category: Natación
Tags: Marcelo, Natación

El pasado 2024/04/28 se celebró en C.N. Helios la competición **Alevín, jornada 6** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **200m Libre** hizo 2:16.64 quedándose 1º
 * En **200m Mariposa** hizo 2:37.87 quedándose 1º
 


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/jdRKUfvzPvc
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/3HeQM3Q554E
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.