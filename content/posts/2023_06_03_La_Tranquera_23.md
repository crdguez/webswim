Title: La Tranquera 23 2022-2023
Date: 2023/06/03
Category: Triatlón
Tags: Marcelo, Triatlón

El pasado 2023/06/03 se celebró en Carenas la competición **La Tranquera 23** de Triathlón donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **Triathlón** hizo 25:34:00 quedándose 6º
 


Aquí os dejamos alguna foto: 

 ![La Tranquera 23]({static}/media/2223/IMG_20230603_170047.jpg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/cZFPYLgsGA4
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.