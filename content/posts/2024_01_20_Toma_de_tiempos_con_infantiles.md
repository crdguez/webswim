Title: Toma de tiempos con infantiles 2023-2024
Date: 2024/01/20
Category: Natación
Tags: Marcelo, Natación

El pasado 2024/01/20 se celebró en E.M. El Olivar la competición **Toma de tiempos con infantiles** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **400m Estilos** hizo 5:34.38 quedándose 1º
 * En **800m Libre** hizo 10:17.08 quedándose 1º
 


Aquí os dejamos alguna foto: 

 ![Toma de tiempos con infantiles]({static}/media/2324/photo_2024-01-22_16-46-52.jpg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/0lv-U5XdfvI
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/YAg_X6TTkvw
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.