Title: Trofeo Agustinos 2023-2024
Date: 2024/05/25
Category: Natación
Tags: Marcelo, Natación

El pasado 2024/05/25 se celebró en P. San Agustin la competición **Trofeo Agustinos** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **400m Estilos** hizo 5:31.01 quedándose 3º
 * En **200m Estilos** hizo 2:35.55 quedándose 1º
 * En **200m Mariposa** hizo 2:36.59 quedándose 1º



Aquí os dejamos alguna foto: 

 ![Trofeo Agustinos]({static}/media/2324/400x_agustinos.jpg){width=50%}
 ![Trofeo Agustinos 2]({static}/media/2324/200x_agustinos.jpg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Oc7pz9ksWTs
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/YLYVJtFyXbQ
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/GdjJHH-Xt_I
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.