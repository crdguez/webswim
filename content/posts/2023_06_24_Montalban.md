Title: Triatlón Hojas Muertas 2022-2023
Date: 2023/06/24
Category: Triatlón
Tags: Marcelo, Carla, Triatlón

El pasado 2023/06/24 se celebró en Montalbán el Triatlón Hojas Muertas donde participaron  Carla y Marcelo.

Aquí os dejamos alguna foto: 

 ![Montalbán 23]({static}/media/2223/IMG_20230624_122614.jpg){width=50%}


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/8Zgl5qROrNw
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.