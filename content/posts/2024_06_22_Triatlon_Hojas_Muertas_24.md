Title: Triatlón Hojas Muertas 24 2023-2024
Date: 2024/06/22
Category: Triatlón
Tags: Carla, Triatlón

El pasado 2024/06/22 se celebró en Montalbán la competición **Triatlón Hojas Muertas 24** de Triatlón donde participó Carla.



Aquí os dejamos alguna foto: 

 ![Triatlón Hojas Muertas 24]({static}/media/2324/photo_2024-07-06_11-06-17.jpg){width=50%}
 ![Triatlón Hojas Muertas 24]({static}/media/2324/IMG_20240622_130754.jpg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/tHTMePReYD4
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.