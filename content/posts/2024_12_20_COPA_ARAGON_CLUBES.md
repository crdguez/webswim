Title: Copa Aragón de Clubes 2024-2025
Date: 2024/12/20
Category: Natación
Tags: Marcelo, Natación

El pasado 2024/12/20 se celebró en E.M. El Olivar la competición **COPA ARAGON CLUBES** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **200m Mariposa** hizo 2:27.85 quedándose 6º
 * En **400m Estilos** hizo 5:14.44 quedándose 9º
 * En **800m Libre** hizo 10:04.21 quedándose 9º
 * En **200m Estilos** hizo 2:28.18 quedándose 7º
 


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/05aW27OgzSU
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/m9VMUpt75GQ
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/cY34ApdLs_w
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Vu3MBKjHn6k
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.