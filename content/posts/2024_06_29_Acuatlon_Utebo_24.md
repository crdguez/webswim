Title: Acuatlón de Utebo 2024
Date: 2024/06/29
Category: Triatlon
Tags: Marcelo, Carla, Triatlon

El pasado 2024/06/29 se celebró en Utebo el Acuatlón donde participaron Marcelo y Carlas.

Aquí os dejamos alguna foto: 

![Acuatlon Utebo]({static}/media/2324/IMG_20240629_112449.jpg){width=50%}
![Acuatlon Utebo2]({static}/media/2324/IMG_20240629_104338.jpg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/1wji1Sr1hpw
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.
