Title: Alevín, jornada 4 2023-2024
Date: 2024/01/28
Category: Natación
Tags: Marcelo, Natación

El pasado 2024/01/28 se celebró en P. San Agustin la competición **Alevín, jornada 4** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **100m Mariposa** hizo 1:14.06 quedándose 1º
 * En **200m Braza** hizo 3:06.38 quedándose 1º
 


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/k52Kj2WdMWY
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/cx5yRa2y6Lc
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.