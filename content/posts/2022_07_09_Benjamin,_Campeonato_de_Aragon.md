Title: Benjamín, Campeonato de Aragón 2021-2022
Date: 2022/07/09
Category: Natación
Tags: Marcelo, Natación

El pasado 2022/07/09 se celebró en E.M. El Olivar la competición **Benjamín, Campeonato de Aragón** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **100m Libre** hizo 1:14.25 quedándose 3º
 * En **100m Espalda** hizo 1:30.03 quedándose 7º
 * En **100m Braza** hizo 1:41.81 quedándose 5º
 * En **100m Mariposa** hizo 1:27.60 quedándose 3º
 * En **200m Estilos** hizo 3:05.72 quedándose 3º
 


Aquí os dejamos alguna foto: 

 ![Benjamín, Campeonato de Aragón]({static}/media/2122/photo_2022-12-07_16-55-13.jpg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/-WDAFkse2Kg
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/h6MXSUS0Abk
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/2kFgt_yPpfw
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/ffRsRA0fniw
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/8T4Z2Qyt7kY
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima, y recuerdaaaaa: **¡Carpe diem!**.