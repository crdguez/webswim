Title: Campeonato de Aragón, Invierno 25 2024-2025
Date: 2025/02/01
Category: Natación
Tags: Marcelo, Natación

El pasado 2025/02/01 se celebró en St. Casablanca la competición **Campeonato de Aragón, Invierno 25** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **400m Estilos** hizo 5:16.61 quedándose 3º
 * En **200m Mariposa** hizo 2:35.52 quedándose 2º
 * En **200m Estilos** hizo 2:33.03 quedándose 3º
 * En **50m Mariposa** hizo 30.58 quedándose 5º
 * En **200m Braza** hizo 2:49.79 quedándose 4º
 


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/CI_DCJau4EI
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/loJJ6HoYfH8
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/2H5FbHrlJm0
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/GgwmrUF8G6c
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/FPTumYJvSfE
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.