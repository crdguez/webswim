Title: Infantil, Jornada 2 2024-2025
Date: 2025/01/18
Category: Natación
Tags: Marcelo, Natación

El pasado 2025/01/18 se celebró en St. Casablanca la competición **Infantil, Jornada 2** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **100m Braza** hizo 1:20.97 quedándose 1º
 * En **200m Libre** hizo 2:21.18 quedándose 4º
 * En **100m Libre** hizo 1:03.32 quedándose 6º
 * En **200m Espalda** hizo 2:32.43 quedándose 1º
 


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/k6XM3bbXkqM
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/1d4XX5-tORk
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/muqDXuh1XjY
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Jx3J1QqnjA4
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.