Title: Benjamín, jornada 2 2022-2023
Date: 2022/12/11
Category: Natación
Tags: Marcelo, Natación

El pasado 2022/12/11 se celebró en P. San Agustin la competición **Benjamín, jornada 2** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **100m Braza** hizo 1:39.16 quedándose 6º
 * En **200m Libre** hizo 2:51.61 quedándose 4º
 


Aquí os dejamos alguna foto: 

 ![Benjamín, jornada 2]({static}/media/2223/IMG_20221211_103330.jpg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/8WmwfsYyw9A
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/FKGCk4zJcno
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima, y recuerdaaaaa: **¡Carpe diem!**.