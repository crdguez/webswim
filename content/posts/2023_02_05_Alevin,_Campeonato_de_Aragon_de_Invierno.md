Title: Alevín, Campeonato de Aragón de Invierno 2022-2023
Date: 2023/02/05
Category: Natación
Tags: Marcelo, Natación

El pasado 2023/02/05 se celebró en E.M. El Olivar la competición **Alevín, Campeonato de Aragón de Invierno** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **200m Estilos** hizo 3:08.21 quedándose 5º
 * En **100m Mariposa** hizo 1:25.82 quedándose 1º
 * En **200m Libre** hizo 2:41.17 quedándose 2º
 


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/T2ctcbhP1cQ
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/EXsjUFiazpE
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/ZJXl8aYVKXM
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.