Title: Campeonato de España Alevín por federaciones 2023-2024
Date: 2024/03/08
Category: Natación
Tags: Marcelo, Natación

El pasado 2024/03/08 se celebró en Los Barrios la competición **Campeonato de España Alevín por federaciones** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **200m Mariposa** hizo 2:42.28 quedándose 14º
 * En **400m Estilos** hizo 5:28.44 quedándose 12º
 * En **200m Braza** hizo 3:00.08 quedándose 19º
 


Aquí os dejamos alguna foto: 

 ![Campeonato de España Alevín por federaciones 1]({static}/media/2324/IMG-20240310-WA0006.jpg){width=50%}
 ![Campeonato de España Alevín por federaciones 2]({static}/media/2324/IMG-20240310-WA0005.jpg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/o1kl_pE1TlE
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/3Nm7P7a9yDQ
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/enrjoJoUoo4
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.