Title: Trofeo San Silvestre 2023 2023-2024
Date: 2023/12/29
Category: Natación
Tags: Marcelo, Natación

El pasado 2023/12/29 se celebró en E.M. El Olivar la competición **Trofeo San Silvestre 2023** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **100m Estilos** hizo 1:16.47 quedándose 2º
 


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Wio_2ElynnE
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.