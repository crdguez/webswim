Title: Benjamín, jornada 2 2021-2022
Date: 2021/12/19
Category: Natación
Tags: Marcelo, Natación

El pasado 2021/12/19 se celebró en St. Casablanca la competición **Benjamín, jornada 2** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **50m Braza** hizo 50.72 quedándose 9º
 * En **100m Libre** hizo 1:26.22 quedándose 8º
 


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/P9DzLILmZLY
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima, y recuerdaaaaa: **¡Carpe diem!**.