Title: Benjamín, jornada 3 2022-2023
Date: 2023/01/21
Category: Natación
Tags: Marcelo, Natación

El pasado 2023/01/21 se celebró en P. San Agustin la competición **Benjamín, jornada 3** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **200m Estilos** hizo 3:02.47 quedándose 3º
 * En **50m Mariposa** hizo 35.22 quedándose 3º
 


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/fHBSh4pWVEY
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/0WYh8Kf8Lt8
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima, y recuerdaaaaa: **¡Carpe diem!**.