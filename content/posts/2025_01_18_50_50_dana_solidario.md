Title: 50x50 Solidario por la Dana 25
Date: 2025/01/18
Category: Natacion	
Tags: Carla, Natacion

El pasado 2025/01/18 Carla estuvo en **Agustinos** nadando los 50x50 solidarios.

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/VhDFOtcyM7E
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.