Title: Alevín, jornada 5 2023-2024
Date: 2024/03/16
Category: Natación
Tags: Marcelo, Natación

El pasado 2024/03/16 se celebró en E.M. El Olivar la competición **Alevín, jornada 5** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **200m Espalda** hizo 2:41.06 quedándose 3º
 * En **800m Libre** hizo 10:15.06 quedándose 1º
 


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/ugvWhhxXBA4
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/-Gy8QHJRkoA
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.