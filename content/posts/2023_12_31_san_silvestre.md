Title: San Silvestre 2023
Date: 2023/12/31
Category: Atletismo	
Tags: Marcelo, Carla, Atletismo

El pasado 2022¡3/12/31 estuvimos en **Sabiñanigo** corriendo la popular San Silvestre.

Los resultados fueron: 

* **Carla** quedó 2ª de categoría benjamín
* **Marcelo** quedó 1º de categoría infantil


Aquí os dejamos alguna foto: 

![Carla y Marcelo]({static}/media/2324/IMG_20231231_185733.jpg){width=50%}
![Carla podio]({static}/media/2324/IMG_20231231_183702.jpg){width=50%}
![Marcelo podio]({static}/media/2324/IMG_20231231_184124.jpg){width=50%}
![Marcelo salida]({static}/media/2324/29.jpeg){width=50%}  - foto de Jacetania Express-

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/FD074HGmUDM
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.