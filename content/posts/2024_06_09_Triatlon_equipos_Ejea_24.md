Title: Triatlón por equipos de Ejea 2024
Date: 2024/06/09
Category: Triatlon
Tags: Marcelo, Triatlon

El pasado 2024/06/09 se celebró en Ejea de los Caballeros el Triatlón por equipos donde participó Marcelo.

Aquí os dejamos alguna foto: 

![Tri Ejea]({static}/media/2324/IMG-20240609-WA0001.jpeg){width=50%}
![Tri Ejea1]({static}/media/2324/IMG-20240609-WA0003.jpg){width=50%}
![Tri Ejea2]({static}/media/2324/IMG-20240609-WA0004.jpg){width=50%}
![Tri Ejea3]({static}/media/2324/IMG-20240610-WA0000.jpg){width=50%}
![Tri Ejea4]({static}/media/2324/IMG-20240610-WA0002.jpg){width=50%}
![Tri Ejea5]({static}/media/2324/IMG-20240610-WA0003.jpg){width=50%}
![Tri Ejea6]({static}/media/2324/IMG-20240610-WA0004.jpg){width=50%}


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/4FSG0jr-uGM
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.
