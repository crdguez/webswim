Title: Un fin de semana en la nieve 2022-2023
Date: 2022/12/05
Category: Miscelánea
Tags: Marcelo, Carla, Miscelánea

El pasado 2022/12/05 estuvimos en Escarrilla pasando **Un fin de semana en la nieve** .


Aquí os dejamos alguna foto: 

 ![Un fin de semana en la nieve]({static}/media/2223/IMG_20221205_152924_1.jpg){width=50%}
 ![Un fin de semana en la nieve]({static}/media/2223/photo_2022-12-07_15-45-11.jpg){width=50%}
 ![Un fin de semana en la nieve]({static}/media/2223/photo_2022-12-07_15-47-42.jpg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/gSYVj4WnX1Y
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima, y recuerdaaaaa: **¡Carpe diem!**.