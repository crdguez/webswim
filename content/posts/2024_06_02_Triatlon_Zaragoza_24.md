Title: Triatlón de Zaragoza 2024
Date: 2024/06/02
Category: Triatlon
Tags: Marcelo, Triatlon

El pasado 2024/06/02 se celebró en Vadorrey el Triatlón de Zaragoza donde participó Marcelo.

Aquí os dejamos alguna foto: 

![Tri Zaragoza]({static}/media/2324/IMG_20240602_091933.jpg){width=50%}


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/jdW273RGpVw
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.
