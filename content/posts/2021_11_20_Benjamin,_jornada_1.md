Title: Benjamín, jornada 1 2021-2022
Date: 2021/11/20
Category: Natación
Tags: Marcelo, Natación

El pasado 2021/11/20 se celebró en C.N. Helios la competición **Benjamín, jornada 1** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **50m Espalda** hizo 43.64 quedándose 9º
 * En **50m Libre** hizo 36.22 quedándose 5º
 


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/ukQDZM_64gY
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/jwVJkjGi-A4
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima, y recuerdaaaaa: **¡Carpe diem!**.