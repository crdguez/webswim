Title: Alevín, jornada 3 2023-2024
Date: 2023/12/02
Category: Natación
Tags: Marcelo, Natación

El pasado 2023/12/02 se celebró en P. San Agustin la competición **Alevín, jornada 3** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **200m Estilos** hizo 2:40.31 quedándose 1º
 * En **100m Libre** hizo 1:05.28 quedándose 1º
 


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/wa5Lf3pHQ7w
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/lN-NZd5mo9o
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.