Title: Benjamín, jornada 3 2021-2022
Date: 2022/01/22
Category: Natación
Tags: Marcelo, Natación

El pasado 2022/01/22 se celebró en St. Venecia la competición **Benjamín, jornada 3** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **100m Estilos** hizo 1:34.18 quedándose 5º
 * En **50m Mariposa** hizo 43.53 quedándose 3º
 


Nos vemos en la próxima, y recuerdaaaaa: **¡Carpe diem!**.