Title: Duatlón Aldea Gala 25
Date: 2025/02/01
Category: Triatlon
Tags: Carla, Triatlon

El pasado 2025/02/01 se celebró en Santa Engracia (Tauste) el Duatlón donde participó Carla.


![Carla podio]({static}/media/2425/IMG_20250201_141651.jpg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/4giaOnFIqn4
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.
