Title: Benjamín, Campeonato base 2023-2024
Date: 2024/06/15
Category: Gimnasia
Tags: Carla, Gimnasia

El pasado 2024/06/15 se celebró en Pepe Garcés la competición **Benjamín, Campeonato base** de Gimnasia donde participó Carla.



Aquí os dejamos alguna foto: 

 ![Benjamín, Campeonato base]({static}/media/2324/IMG_20240616_122028.jpg){width=50%}
 ![Benjamín, Campeonato base]({static}/media/2324/IMG_20240616_122535.jpg){width=50%}
 ![Benjamín, Campeonato base]({static}/media/2324/IMG_20240616_122812.jpg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/W4ZqtLupe2k
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.