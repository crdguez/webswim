Title: San Silvestre 2022
Date: 2022/12/31
Category: Atletismo	
Tags: Marcelo, Carla, Atletismo

El pasado 2022/12/31 estuvimos en **Sabiñanigo** corriendo la popular San Silvestre.

Los resultados fueron: 

* **Carla** quedó 3ª de categoría benjamín
* **Marcelo** quedó 3º de categoría infantil


Aquí os dejamos alguna foto: 

![Carla y Cecilia]({static}/media/2223/IMG_20221231_165836.jpg){width=50%}
![Marcelo y Guzmán]({static}/media/2223/IMG_20221231_172116.jpg){width=50%}
![Carla podio]({static}/media/2223/IMG_20221231_183656.jpg){width=50%}
![Marcelo podio]({static}/media/2223/IMG_20221231_184302.jpg){width=50%}


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/COHYUrhCyhA
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.