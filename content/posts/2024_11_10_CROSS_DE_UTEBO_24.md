Title: Cross de Utebo 24
Date: 2024/11/10
Category: Atletismo
Tags: Marcelo, Atletismo

El pasado 2024/11/10 se celebró en Utebo la competición **CROSS DE UTEBO** de Atletismo donde participó Marcelo.

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/b6cfHjORpOw
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.