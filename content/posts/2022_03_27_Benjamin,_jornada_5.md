Title: Benjamín, jornada 5 2021-2022
Date: 2022/03/27
Category: Natación
Tags: Marcelo, Natación

El pasado 2022/03/27 se celebró en P. San Agustin la competición **Benjamín, jornada 5** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **100m Espalda** hizo 1:33.59 quedándose 9º
 * En **50m Mariposa** hizo 38.25 quedándose 2º
 


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/a5x22-cGs3c
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/QqSRqkJg0X8
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima, y recuerdaaaaa: **¡Carpe diem!**.