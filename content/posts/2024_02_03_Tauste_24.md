Title: Duatlón Aldea Gala - Santa Engracia - Tauste
Date: 2024/02/03
Category: Triatlon
Tags: Marcelo, Carla, Triatlon

El pasado 2024/02/03 se celebró en Tauste el Duatlón escolar donde participaron Marcelo y Carla.

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/_at7k_pJcPU
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.
