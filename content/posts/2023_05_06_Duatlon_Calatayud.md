Title: Duatlón Escolar de Calatayud 23
Date: 2023/05/06
Category: Triatlón
Tags: Carla, Triatlón

El pasado 2023/05/06 se celebró  el **Duatlón Escolar de Calatayud** donde participó Carla.

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/9aco1h8nVg8
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.