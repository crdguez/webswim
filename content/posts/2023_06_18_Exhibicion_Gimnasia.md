Title: Exhibición Gimnasia Junio 23
Date: 2023/06/18
Category: Gimnasia
Tags: Carla, Gimnasia

El pasado 2023/06/18 se celebró  la **Exhibición de Gimnasia** donde participó Carla.

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/yDlh98hvqgE
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.