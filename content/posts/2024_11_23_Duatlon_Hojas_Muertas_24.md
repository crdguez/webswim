Title: Duatlón Hojas Muertas 2024
Date: 2024/11/23
Category: Triatlon
Tags: Marcelo, Carla, Triatlon

El pasado 2024/11/23 se celebró en Montalbán el Duatlón donde participaron Marcelo y Carla.

Aquí os dejamos alguna foto: 

![Duatlon Hojas Muertas]({static}/media/2425/IMG_20241123_131222.jpg){width=50%}


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/2LKqZ-aB4EU
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.
