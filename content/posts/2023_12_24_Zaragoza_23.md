Title: Cross de Zaragoza 23 
Date: 2023/12/23
Category: Atletismo
Tags: Marcelo, Atletismo

El pasado 2023/12/23 se celebró en Sabiñanigo el cross escolar donde participó Marcelo.


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Fr2Dwy8v1XY
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.
