Title: Alevín, Campeonato de Aragón de verano 2023-2024
Date: 2024/06/21
Category: Natación
Tags: Marcelo, Natación

El pasado 2024/06/21 se celebró en E.M. El Olivar la competición **Alevín, Campeonato de Aragón** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **100m Mariposa** hizo 1:08.06 quedándose 2º
 * En **200m Estilos** hizo 2:30.22 quedándose 1º
 * En **50m Mariposa** hizo 30.65 quedándose 2º
 * En **400m Libre** hizo 4:43.40 quedándose 2º
 


Aquí os dejamos alguna foto: 

 ![Alevín, Campeonato de Aragón]({static}/media/2324/IMG_20241209_094634_980.jpg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/mmc9HPFuAF4
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/2bI2UrBOQV4
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/YszUZ9NqBHw
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/4dps2HP2Vfs
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.