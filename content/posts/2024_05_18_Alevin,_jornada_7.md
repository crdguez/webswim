Title: Alevín, jornada 7 2023-2024
Date: 2024/05/18
Category: Natación
Tags: Marcelo, Natación

El pasado 2024/05/18 se celebró en E.M. El Olivar la competición **Alevín, jornada 7** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **400m Estilos** hizo 5:18.90 quedándose 1º
 * En **100m Libre** hizo 1:03.54 quedándose 2º
 


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/WtXU03bOY5Y
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/WU60Nuh26Y4
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.