Title: Duatlón de Utebo 23 2023-2024
Date: 2023/10/01
Category: Triatlón
Tags: Marcelo, Carla, Triatlón

El pasado 2023/10/01 se celebró en Utebo el Duatlón escolar donde participó Marcelo y Carla.

Aquí os dejamos alguna foto: 

 ![Utebo]({static}/media/2324/IMG-20231001-WA0048.jpg){width=50%}
 ![Utebo]({static}/media/2324/IMG-20231001-WA0031.jpg){width=50%}
 ![Utebo]({static}/media/2324/IMG_20231001_134951.jpg){width=50%}


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/bnWqBBSpAK0
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.