Title: Benjamín, Campeonato Escolar Provincial 2023-2024
Date: 2024/02/24
Category: Gimnasia
Tags: Carla, Gimnasia

El pasado 2024/02/24 se celebró en Pepe Garcés la competición **Benjamín, Campeonato Escolar Provincial** de Gimnasia donde participó Carla.

Aquí os dejamos alguna foto: 

 ![Benjamín, Campeonato Escolar Provincial]({static}/media/2324/IMG_20240225_142411.jpg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/umzG9O9Ta30
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.