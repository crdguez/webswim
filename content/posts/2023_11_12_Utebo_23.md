Title: Cross de Utebo 23 2023-2024
Date: 2023/11/12
Category: Atletismo
Tags: Marcelo, Carla, Atletismo

El pasado 2023/11/12 se celebró en Utebo el cross escolar donde participó Marcelo y Carla.

Aquí os dejamos alguna foto: 

 ![Utebo]({static}/media/2324/carla_12_11_23.png){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/yJQrOCQIfLM
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.