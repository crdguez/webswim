Title: Benjamín, jornada 4 2022-2023
Date: 2023/02/19
Category: Natación
Tags: Marcelo, Natación

El pasado 2023/02/19 se celebró en P. San Agustin la competición **Benjamín, jornada 4** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **200m Libre** hizo 2:38.23 quedándose 3º
 * En **200m Braza** hizo 3:31.22 quedándose 4º
 * En **50m Mariposa** hizo 35.38 quedándose 2º
 


Aquí os dejamos alguna foto: 

 ![Benjamín, jornada 4]({static}/media/2223/IMG_20230219_090219.jpg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/MME_LsLkJxg
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/PvW1CHrA3pQ
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/P0otwHHd8SE
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.