Title: Triangular de Gimnasia Artística
Date: 2024/04/27
Category: Gimnasia
Tags: Carla, Gimnasia

El pasado 2024/04/27 se celebró en Pepe Garcés la competición **Triangular de Gimnasia Artística** de Gimnasia donde participó Carla.

Aquí os dejamos alguna foto: 

 ![Benjamín, Campeonato Escolar Provincial]({static}/media/2324/IMG_20240225_142411.jpg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/oDCiWfztmEc
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.