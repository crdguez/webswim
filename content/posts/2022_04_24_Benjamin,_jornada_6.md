Title: Benjamín, jornada 6 2021-2022
Date: 2022/04/24
Category: Natación
Tags: Marcelo, Natación

El pasado 2022/04/24 se celebró en E.M. El Olivar la competición **Benjamín, jornada 6** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **200m Libre** hizo 2:52.84 quedándose 4º
 


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/oSyJL2RBGxY
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima, y recuerdaaaaa: **¡Carpe diem!**.