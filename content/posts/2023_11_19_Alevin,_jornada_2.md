Title: Alevín, jornada 2 2023-2024
Date: 2023/11/19
Category: Natación
Tags: Marcelo, Natación

El pasado 2023/11/19 se celebró en C.N. Helios la competición **Alevín, jornada 2** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **400m Libre** hizo 5:02.12 quedándose 1º
 * En **100m Espalda** hizo 1:16.29 quedándose 1º
 


Aquí os dejamos alguna foto: 

 ![Alevín, jornada 2]({static}/media/2324/photo_2023-11-19_11-42-06.jpg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/x2aKC8dQF9g
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/WpfD2iOPj7E
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.