Title: Trofeo Fernando López 2022-2023
Date: 2023/05/07
Category: Natación
Tags: Marcelo, Natación

El pasado 2023/05/07 se celebró en C. N. Badalona la competición **Trofeo Fernando López** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **100m Mariposa** hizo 1:19.89 quedándose 9º
 * En **100m Libre** hizo 1:08.54 quedándose 9º
 


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/32z4S7ey6Hc
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.