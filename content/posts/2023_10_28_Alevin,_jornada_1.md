Title: Alevín, jornada 1 2023-2024
Date: 2023/10/28
Category: Natación
Tags: Marcelo, Natación

El pasado 2023/10/28 se celebró en E.M. El Olivar la competición **Alevín, jornada 1** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **200m Libre** hizo 2:22.31 quedándose 1º
 


Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/jBnOcy-iOk0
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.