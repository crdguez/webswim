Title: Benjamín, Campeonato de Aragón 2022-2023
Date: 2023/07/08
Category: Natación
Tags: Marcelo, Natación

El pasado 2023/07/08 se celebró en E.M. El Olivar la competición **Benjamín, Campeonato de Aragón** de Natación donde participó Marcelo.


Los resultados obtenidos fueron: 

 * En **100m Libre** hizo 1:07.37 quedándose 2º
 * En **200m Estilos** hizo 2:50.72 quedándose 2º
 * En **100m Mariposa** hizo 1:17.09 quedándose 2º
 * En **100m Espalda** hizo 1:21.26 quedándose 6º
 * En **100m Braza** hizo 1:31.24 quedándose 4º
 


Aquí os dejamos alguna foto: 

 ![Benjamín, Campeonato de Aragón]({static}/media/2223/IMG_20230708_110414.jpg){width=50%}

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/HyPV4oQWKkI
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/9F_d5SR2puM
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/83gROsl7X44
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/FjyYUxBi-uo
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/iF8jG7-p2QQ
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.