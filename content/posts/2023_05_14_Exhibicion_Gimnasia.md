Title: Exhibición Gimnasia Mayo 23
Date: 2023/05/14
Category: Gimnasia
Tags: Carla, Gimnasia

El pasado 2023/05/14 se celebró  la **Exhibición de Gimnasia** donde participó Carla.

Hemos grabado algún vídeo del evento: 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/u-y5BRyX18s
                " frameborder="0" allow="accelerometer; encrypted-media; gyroscope; 
                picture-in-picture" allowfullscreen></iframe>

Nos vemos en la próxima.