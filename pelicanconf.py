#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Carlos Rodríguez'
SITENAME = '¡Bitácora de una familia!'
SITESUBTITLE = 'Un cajón de sastre con los eventos de la familia.'

SITEURL = ''


PATH = 'content'

TIMEZONE = 'Europe/Athens'

DEFAULT_LANG = 'es'

# GITHUB_URL = 'http://github.com/crdguez/'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Resultados de natación', 'https://crdguez-dashboard-natacion-main-j8ammt.streamlit.app/'),
	('Calendario fan','https://fanaragon.com/252950-2/'),
	('Calendario fatri ','http://www.triatlonaragon.org/Competiciones/Calendario')
        # ('Pelican', 'https://getpelican.com/'),
         #('Python.org', 'https://www.python.org/'),
        # ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         #('You can modify those links in your config file', '#'),
         )

# Social widget
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)

DISPLAY_PAGES_ON_MENU = True

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

# comenta si quieres el tema por defecto
#THEME='/tmp/tuxlite_tbs'
#THEME='/tmp/pelican-themes/Peli-Kiera'
